﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using ArchiveIt.ViewModels;
using System.Management.Automation;
using System.Web;

namespace ArchiveIt.Controllers
{
    public class ArchiveController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/check
        [HttpPost]
        public HttpResponseMessage Post(ArchiveData data)
        {
            var response = Request.CreateResponse(HttpStatusCode.NotFound, "Company name and Password must be entered!");

            if (string.IsNullOrEmpty(data.CompanyName) || string.IsNullOrEmpty(data.Password)) return response;

            try
            {
                var checkoutFolder = WebConfigurationManager.AppSettings["checkoutFolder"];
                var archiveFileName = WebConfigurationManager.AppSettings["archiveFile"];
                var powershellDir = HttpContext.Current.Server.MapPath("~/App_Code") + "\\Powershell";
                string execArchiveScript = string.Format(powershellDir + "\\ArchiveFolder.ps1 '{0}' '{1}' '{2}' '{3}' '{4}'", checkoutFolder, powershellDir, data.CompanyName, data.Password, archiveFileName);
                
                // Initialize PowerShell engine
                var shell = PowerShell.Create();
                
                // Archive folder content
                shell.Commands.Clear();
                shell.Commands.AddScript(execArchiveScript);
                shell.Invoke();


                response = Request.CreateResponse(HttpStatusCode.OK,
                        "Repository content archived successfully!");

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

    }
}
