﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using ArchiveIt.DTO;
using ArchiveIt.ViewModels;
using LibGit2Sharp;
using System.Management.Automation;
using System.Web;
using ArchiveIt.Helpers;

namespace ArchiveIt.Controllers
{
    public class CheckController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/check
        [HttpPost]
        public HttpResponseMessage Post(PostData data)
        {
            var response = Request.CreateResponse(HttpStatusCode.NotFound, "GIT Repo Username and Password must be entered!");

            if (string.IsNullOrEmpty(data.Username) || string.IsNullOrEmpty(data.Password)) return response;

            try
            {
                var sourceUrl = WebConfigurationManager.AppSettings["repo"];
                var checkoutFolder = WebConfigurationManager.AppSettings["checkoutFolder"];
                var powershellDir = HttpContext.Current.Server.MapPath("~/App_Code") + "\\Powershell";
                string execDeleteFolderScript = string.Format(powershellDir + "\\DeleteFolderContent.ps1 '{0}'", checkoutFolder);

                CloneOptions options = new CloneOptions()
                {
                    CredentialsProvider = (url, user, cred) => new UsernamePasswordCredentials()
                    {
                        Username = data.Username,
                        Password = data.Password
                    },
                    BranchName = WebConfigurationManager.AppSettings["branch"]
                };
                
                // Initialize PowerShell engine
                var shell = PowerShell.Create();

                // Delete content of checkout folder
                shell.Commands.AddScript(execDeleteFolderScript);
                shell.Invoke();
                               
                // Clone repository
                response = CloneRepository(sourceUrl, checkoutFolder, options, response);

            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return response;
        }

        private HttpResponseMessage CloneRepository(string sourceUrl, string checkoutFolder, CloneOptions options,
            HttpResponseMessage response)
        {
            System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(checkoutFolder);

            Empty(directory); // temp fix for special case when code has been checked out, but not deleted after creating zip

            if (FilesAndDir.IsDirectoryEmpty(checkoutFolder))
            {
                string gitFolder = Repository.Clone(sourceUrl, checkoutFolder, options);
                if (!string.IsNullOrEmpty(gitFolder))
                {
                    response = Request.CreateResponse(HttpStatusCode.OK,
                        "Repository has been checked out successfully!");
                }
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError,
                    "Checkout Folder Is Not Empty!");
            }
            return response;
        }

        public static void Empty(System.IO.DirectoryInfo directory)
        {
            foreach (System.IO.FileInfo file in directory.GetFiles()) file.Delete();
            foreach (System.IO.DirectoryInfo subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
        }
        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

    }
}
