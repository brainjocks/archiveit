﻿using System.IO;
using System.Management.Automation;
using System.Web.Configuration;
using System.Web.Mvc;

namespace ArchiveIt.Controllers
{
    public class DownloadController : Controller
    {
        // GET api/values
        public ActionResult Get()
        {
            // **************************************************
            var powershellDir = System.Web.HttpContext.Current.Server.MapPath("~/App_Code") + "\\Powershell";
            var checkoutFolder = WebConfigurationManager.AppSettings["checkoutFolder"];
            string execDeleteFolderScript = string.Format(powershellDir + "\\DeleteFolderContent.ps1 '{0}'", checkoutFolder);
            var archiveFileName = WebConfigurationManager.AppSettings["archiveFile"];

            string strPathName = checkoutFolder + "\\" + archiveFileName;

            if (System.IO.File.Exists(strPathName) == false)
            {
                return new EmptyResult();
            }
            // **************************************************

            Stream oStream = null;

            try
            {
                // Open the file
                oStream =
                    new System.IO.FileStream
                        (path: strPathName,
                        mode: System.IO.FileMode.Open,
                        share: System.IO.FileShare.Read,
                        access: System.IO.FileAccess.Read);

                // **************************************************
                Response.Buffer = false;

                // Setting the unknown [ContentType]
                // will display the saving dialog for the user
                Response.ContentType = "application/octet-stream";

                // With setting the file name,
                // in the saving dialog, user will see
                // the [strFileName] name instead of [download]!
                Response.AddHeader("Content-Disposition", "attachment; filename=" + archiveFileName);

                long lngFileLength = oStream.Length;

                // Notify user (client) the total file length
                Response.AddHeader("Content-Length", lngFileLength.ToString());
                // **************************************************

                // Total bytes that should be read
                long lngDataToRead = lngFileLength;

                // Read the bytes of file
                while (lngDataToRead > 0)
                {
                    // The below code is just for testing! So we commented it!
                    //System.Threading.Thread.Sleep(200);

                    // Verify that the client is connected or not?
                    if (Response.IsClientConnected)
                    {
                        // 8KB
                        int intBufferSize = 8 * 1024;

                        // Create buffer for reading [intBufferSize] bytes from file
                        byte[] bytBuffers =
                            new System.Byte[intBufferSize];

                        // Read the data and put it in the buffer.
                        int intTheBytesThatReallyHasBeenReadFromTheStream =
                            oStream.Read(buffer: bytBuffers, offset: 0, count: intBufferSize);

                        // Write the data from buffer to the current output stream.
                        Response.OutputStream.Write
                            (buffer: bytBuffers, offset: 0,
                            count: intTheBytesThatReallyHasBeenReadFromTheStream);

                        // Flush (Send) the data to output
                        // (Don't buffer in server's RAM!)
                        Response.Flush();

                        lngDataToRead =
                            lngDataToRead - intTheBytesThatReallyHasBeenReadFromTheStream;
                    }
                    else
                    {
                        // Prevent infinite loop if user disconnected!
                        lngDataToRead = -1;
                    }
                }
            }
            catch { }
            finally
            {
                if (oStream != null)
                {
                    //Close the file.
                    oStream.Close();
                    oStream.Dispose();
                    oStream = null;
                }
                Response.Close();

                //Clean folder?
                // Initialize PowerShell engine
                var shell = PowerShell.Create();

                // Delete content of checkout folder
                shell.Commands.AddScript(execDeleteFolderScript);
                shell.Invoke();
            }

            return new EmptyResult();
        }
    }
}