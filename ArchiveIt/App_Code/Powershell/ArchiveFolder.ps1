﻿Function ArchiveFolder($params)
{
    cls

	# Define dir invocation path
	$source = $params[0]
	$directorypath = $params[1]
	$company = $params[2]
	$archivePassword = $params[3]
	$archiveName = $params[4]

	# Import utils module
	Import-Module -Name ($directorypath + "\utils.psm1") -Verbose

	# Read config values
	[xml]$config = Get-Content ($directorypath + "\start.config")
	$placeholder  = $config.Configuration.General.Placeholder
	$insertionText1  = $config.Configuration.General.InsertionText1
	$insertionText2  = $config.Configuration.General.InsertionText2
	$insertionText3  = $config.Configuration.General.InsertionText3

	# Set variables
	$destination = $source + "\" + $archiveName
	$sitecoreLibs = $source + "\Libs\Sitecore"

	# Remove existing zip file
	If(Test-path $destination) {Remove-item $destination}

	# Delete .git files and folders. Delete sitecore LIBS
	Write-Host "Removing all .git files and folders..."
	Get-ChildItem $source -Recurse -Force -Filter .git* | Remove-Item -Force -Recurse
	Write-Host "Finished deleting .git files and folders!"

	
	If(Test-path $sitecoreLibs) {
		Write-Host "Removing Sitecore Libs..."
		Get-ChildItem $sitecoreLibs -include *.dll,*.xml -recurse | Remove-Item -Force
		Write-Host "Finished deleting Sitecore Libs"
	}
	
	# Replace strings in files (with extensions)
	$filesForReplace = Get-ChildItem $source -Recurse -Force -Include @("*.cs","*.cshtml","*.less","*.js") | Where-Object {!$_.PSIsContainer}
	$currentYear = get-date -Format yyyy;
	$currentDate = get-date -Format "MM/dd/yyyy hh:mm";
	$temp = @();
	$temp += $insertionText1
	$temp += $insertionText2
	$temp += $insertionText3
	$insertionText = ($temp | Out-String).Trim()

	$textToBeInserted = $insertionText -f $currentYear, $company, $currentDate
	Write-Host $textToBeInserted
	foreach ($file in $filesForReplace)
	{
		(Get-Content $file.PSPath) |
		Foreach-Object { $_ -replace $placeholder, $textToBeInserted } |
		Set-Content $file.PSPath
	}

	# Create zip file from source folder
	Write-Host "Creating archive from source '$source', with password '$archivePassword' and destination file '$destination'" -ForegroundColor DarkGray

	# [io.compression.zipfile]::CreateFromDirectory($Source, $destination)
	if ($archivePassword){
		Write-ZipUsing7Zip -FilesToZip $source -ZipOutputFilePath $destination -Password $archivePassword -HideWindow
	}else{
		Write-ZipUsing7Zip -FilesToZip $source -ZipOutputFilePath $destination -HideWindow
	}

	Write-Host "Archive created!"
}

if ($args.Length -eq 0)
{
    echo "Usage: ArchiveFolder <sourceDir> <executionDir> <company> <archiveName>"
}
else
{
    ArchiveFolder($args[0], $args[1], $args[2], $args[3], $args[4])
}