﻿Function DeleteFolderContent($source)
{
    Get-ChildItem $source -Recurse -Force | Remove-Item -Force -Recurse
}

if ($args.Length -eq 0)
{
    echo "Usage: DeleteFolderContent <directory>"
}
else
{
    DeleteFolderContent($args[0])
}