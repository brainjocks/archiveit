﻿define(['jquery', 'knockout', 'komapping', 'underscore', 'spin'], function ($, ko, komapping, _, Spinner) {
	var opts = {
		lines: 13 // The number of lines to draw
	, length: 28 // The length of each line
	, width: 14 // The line thickness
	, radius: 42 // The radius of the inner circle
	, scale: 1 // Scales overall size of the spinner
	, corners: 1 // Corner roundness (0..1)
	, color: '#000' // #rgb or #rrggbb or array of colors
	, opacity: 0.25 // Opacity of the lines
	, rotate: 0 // The rotation offset
	, direction: 1 // 1: clockwise, -1: counterclockwise
	, speed: 1 // Rounds per second
	, trail: 60 // Afterglow percentage
	, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
	, zIndex: 2e9 // The z-index (defaults to 2000000000)
	, className: 'spinner' // The CSS class to assign to the spinner
	, top: '50%' // Top position relative to parent
	, left: '50%' // Left position relative to parent
	, shadow: false // Whether to render a shadow
	, hwaccel: false // Whether to use hardware acceleration
	, position: 'absolute' // Element positioning
	}

	var appTarget = document.getElementById('application-layout');
	var spinner = new Spinner(opts);
	var processViewModel = null;
	ko.mapping = komapping;

	ko.bindingHandlers['class'] = {
		update: function (element, valueAccessor) {
			var currentValue = ko.utils.unwrapObservable(valueAccessor()),
				prevValue = element['__ko__previousClassValue__'],

				// Handles updating adding/removing classes
				addOrRemoveClasses = function (singleValueOrArray, shouldHaveClass) {
					if (Object.prototype.toString.call(singleValueOrArray) === '[object Array]') {
						ko.utils.arrayForEach(singleValueOrArray, function (cssClass) {
							var value = ko.utils.unwrapObservable(cssClass);
							ko.utils.toggleDomNodeCssClass(element, value, shouldHaveClass);
						});
					} else if (singleValueOrArray) {
						ko.utils.toggleDomNodeCssClass(element, singleValueOrArray, shouldHaveClass);
					}
				};

			// Remove old value(s) (preserves any existing CSS classes)
			addOrRemoveClasses(prevValue, false);

			// Set new value(s)
			addOrRemoveClasses(currentValue, true);

			// Store a copy of the current value
			element['__ko__previousClassValue__'] = currentValue.concat();
		}
	};

	ko.bindingHandlers.enterkey = {
		init: function (element, valueAccessor, allBindings, viewModel) {
			var callback = valueAccessor();
			$(element).keypress(function (event) {
				var keyCode = (event.which ? event.which : event.keyCode);
				if (keyCode === 13) {
					callback.call(viewModel);
					return false;
				}
				return true;
			});
		}
	};

	function Check(credentials, archive) {
		var self = this;

		self.Credentials = ko.mapping.fromJS(credentials);
		self.Archive = ko.mapping.fromJS(archive);

		//validation
		self.Credentials.Username.extend({ required: true });
		self.Credentials.Password.extend({ required: true });
		self.Archive.CompanyName.extend({ required: true });
		self.Archive.Password.extend({ required: true });

		self.submitInProgress = ko.observable(false);
		self.readyToBeChecked = ko.computed(function () {
			return self.Credentials.Username.isValid() && self.Credentials.Password.isValid() && !self.submitInProgress();
		}, self);

		self.readyToBeArchived = ko.computed(function () {
			return self.Archive.CompanyName.isValid() && self.Archive.Password.isValid() && !self.submitInProgress();
		}, self);

		self.iconCheck = ko.observable("");
		self.iconArchive = ko.observable("");
		self.overallCheck = ko.observableArray(["", ""]);
		self.overallArchive = ko.observableArray(["", ""]);

		self.shouldShowArchive = ko.observable(false);
		self.shouldShowCheck = ko.observable(true);
		self.shouldShowDownload = ko.observable(false);
		self.setFocus = ko.observable(false);

		self.checkOne = function () {
			if (!self.readyToBeChecked())
				return;

			//prevent multiple submit
			self.submitInProgress(true);
			spinner.spin(appTarget);
			//add overlay class
			dim();

			var dataObject = ko.mapping.toJS(self.Credentials);

			return $.post('/api/check', dataObject)
				.done(function (text,type,jqXhr) {
					self.iconCheck("glyphicon-ok");
					self.overallCheck(["has-feedback", "has-success"]);
					appendSucccesStatusMessage(type + ": " + text);

					//show segment for zipping source
					self.shouldShowArchive(true);
					self.setFocus(true);
				})
				.fail(function (jqXhr, textStatus, errorThrown) {
					self.iconCheck("glyphicon-remove");
					self.overallCheck(["has-feedback", "has-error"]);
					appendErrorStatusMessage(errorThrown + ": " + jqXhr.responseText);
				})
				.always(function (e) {
					self.submitInProgress(false);
					spinner.stop();
					unDim();
				});
		}

		self.archiveFolder = function () {
			if (!self.readyToBeArchived())
				return;

			//prevent multiple submit
			self.submitInProgress(true);
			spinner.spin(appTarget);
			//add overlay class
			dim();

			var dataObject = ko.mapping.toJS(self.Archive);

			return $.post('/api/archive', dataObject)
				.done(function (text, type, jqXhr) {
					self.iconArchive("glyphicon-ok");
					self.overallArchive(["has-feedback", "has-success"]);
					appendSucccesStatusMessage(type + ": " + text);

					//show segment for downloading
					self.shouldShowArchive(false);
					self.shouldShowCheck(false);
					self.shouldShowDownload(true);
				})
				.fail(function (jqXhr, textStatus, errorThrown) {
					self.iconArchive("glyphicon-remove");
					self.overallArchive(["has-feedback", "has-error"]);
					appendErrorStatusMessage(errorThrown + ": " + jqXhr.responseText);
				})
				.always(function (e) {
					self.submitInProgress(false);
					spinner.stop();
					unDim();
				});
		}

		self.resetCheck = function () {
			self.Credentials.Username("");
			self.Credentials.Password("");
			self.iconCheck("");
			self.overallCheck(["", ""]);
			self.Credentials.Username.isModified(false);
			self.Credentials.Password.isModified(false);

		}

		self.resetArchive = function () {
			self.Archive.CompanyName("");
			self.Archive.Password("");
			self.iconArchive("");
			self.overallArchive(["", ""]);
			self.Archive.CompanyName.isModified(false);
			self.Archive.Password.isModified(false);
		}
	}

	function appendErrorStatusMessage(value) {
		$('#serverStatus').append("<li class='error-status-message'>" + getDateTime() + " - " + value + "</li>");
	}

	function appendSucccesStatusMessage(value) {
		$('#serverStatus').append("<li class='success-status-message'>" + getDateTime() + " - " + value + "</li>");
	}

	function dim() {
		return $("#dim_wrapper").animate({
			'opacity': 0.5,
			'z-index' : 10
		});
	}

	function unDim() {
		return $("#dim_wrapper").animate({
			'opacity': 0.0,
			'z-index': -10
		});
	}

	function getDateTime() {
		var now = new Date();
		var year = now.getFullYear();
		var month = now.getMonth() + 1;
		var day = now.getDate();
		var hour = now.getHours();
		var minute = now.getMinutes();
		var second = now.getSeconds();
		if (month.toString().length === 1) {
			month = '0' + month;
		}
		if (day.toString().length === 1) {
			day = '0' + day;
		}
		if (hour.toString().length === 1) {
			hour = '0' + hour;
		}
		if (minute.toString().length === 1) {
			minute = '0' + minute;
		}
		if (second.toString().length === 1) {
			second = '0' + second;
		}
		var dateTime = year + '/' + month + '/' + day + ' ' + hour + ':' + minute + ':' + second;
		return dateTime;
	}

	$.fn.pressEnter = function (fn) {
		return this.each(function () {
			$(this).bind('enterPress', fn);
			$(this).keyup(function (e) {
				if (e.keyCode === 13) {
					$(this).trigger("enterPress");
				}
			});
		});
	};

	// create index view view model which contain two models for partial views
	processViewModel = { checkViewModel: new Check({ Username: "", Password: "" }, { Password: "", CompanyName: "" }) };
	return processViewModel;
});