﻿using System.IO;
using System.Linq;

namespace ArchiveIt.Helpers
{
    public static class FilesAndDir
    {
        public static void CleanDirectory(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Attributes = FileAttributes.Normal;
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                CleanDirectory(dir.ToString());
            }
        }

        public static bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }
    }
}